const {dailyDb} = require('../DB.js');
const {outPut}=require('./outPut');

exports.done=(habit)=>{
dailyDb.get(habit, function (err) {
    new Promise((resolve, reject) => {
            if (err) reject(err);
            resolve(habit);
        })
        .then((key) => dailyDb.put(key, 'done'))
        .catch((err) => outPut(err.toString(), "pallet", "red"));
})
}