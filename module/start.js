const figlet = require("figlet");
const chalk = require("chalk");
const {outPut} = require('./outPut');

exports.start=()=> {
    figlet("My Habit !!", function (err, data) {
        if (err) {
            outPut(err, "pallet", "red")  
        }
        console.log(chalk.black.bgYellowBright(data));
    });
}