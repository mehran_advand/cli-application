const {reportsDB} = require('../DB.js');
const {outPut} = require('./outPut');

exports.day=()=>{
  reportsDB.get('day', (err, value) => {
      if (err) outPut(err.toString(), "pallet", "red")
      outPut(`you are in day number ${value.toString("utf8")}`, "tiny", "green")
  })
}