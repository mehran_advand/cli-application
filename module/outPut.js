const CFonts = require("cfonts");

exports.outPut=(text,type,color)=>{
    CFonts.say(text, {
        font: type,
        align: "center",
        colors: [color],
        background: "transparent",
    })
} 
