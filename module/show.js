const Table = require("cli-table3");
const chalk = require("chalk");
const {dailyDb} = require('../DB.js');

exports.show=()=>{
   const table = new Table({
       head: ['Habit', 'Status'],
       colWidths: [50, 50]
   })

   dailyDb.put('~', '~');
   new Promise((resolve, reject) => {
           dailyDb.createReadStream().on("data", function (data) {
               new Promise((resolve, reject) => {
                   resolve([data.key.toString("utf8"), data.value.toString("utf8")]);
               }).then((arr) => {
                   if (data.key != "~") table.push(arr)
                   else return resolve(table);
               });
           });
       })
       .then(table => console.log(chalk.yellow(table.toString())));
}