const {dailyDb} = require('../DB.js');
const {reportsDB} = require('../DB.js');
const {outPut}=require('./outPut');
const Promise = require("bluebird");
const r = require("ramda");

exports.add=(array)=> {
    let list = r.split("/", array);
    Promise.map(list, (element) => {
        reportsDB.put(element, 0);
        dailyDb.put(element, "undone");
    }).then(outPut("habits are added", "block", "green"));
    reportsDB.put('day', 1);
}