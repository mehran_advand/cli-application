const {reportsDB} = require('../DB.js');
const clui = require('clui');
const chalk = require("chalk");

exports.result=()=>{
  reportsDB.createReadStream()
      .on('data', data => {
          let gauge = clui.Gauge;
          let total = 7;
          let done = Number(data.value);
          let suffix = Math.floor(done / total * 100) + '%';
          console.log(chalk.cyan(data.key.toString('utf8')), gauge(done, total, 14, 6, suffix))
      })
}