const {dailyDb} = require('../DB.js');
const {reportsDB} = require('../DB.js');
const {outPut}=require('./outPut');
const{result}=require('./result')

exports.next=(day)=>{
  reportsDB.get('day', (err, value) => {
      if (err) outPut(err.toString(), "pallet", "red");

      else if (value < day && day < 8) {
          reportsDB.put('day', day)
          dailyDb.createReadStream()
              .on('data', (data) => {
                  if (data.value.toString('utf8') === 'done') {
                      reportsDB.get(data.key, (err, value) => {
                          if (err) outPut(err.toString(), "pallet", "red");
                          reportsDB.put(data.key, ++value)
                      })
                  }
                  dailyDb.put(data.key, 'undone')
              })
      } else if (day > 7) {
          result();
          dailyDb.clear();
          reportsDB.clear();
      } else {
          console.log(`you past that day you are in day number ${value}`)
          outPut(`you past that day you are in day number ${value}`, "grid", "yellow");
      }

  })
}