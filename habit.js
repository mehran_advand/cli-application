#!/usr/bin/env node

//packages
const {program}=require('commander');
const  {add}=require('./module/add');
const {start} = require('./module/start');
const {done} = require('./module/done');
const {show} = require('./module/show');
const {result} = require('./module/result');
const {day} = require('./module/day');
const {next}=require('./module/next')

//program option
program.version('0.0.1');
program
    .option('-st,--start', 'show the brand')
    .option(
        "-a, --add <items>",
        "slash separated list",
    )
    .option("-s,--show ", "showing the status")
    .option("-d,--done <habit>", "when dine a daily habit")
    .option("-nd,--next <day>", "it go to next day an ")
    .option("-r,--result", "report user function")
    .option("-d,--day", "show that you are in which day")
    .parse(process.argv);

if (program.add) add(program.add);
else if(program.start) start();
else if (program.show) show();
else if (program.done) done(program.done);
else if (program.next) next(program.next);
else if (program.result) result();
else if (program.day) day();