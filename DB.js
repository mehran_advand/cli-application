const levelup = require("levelup");
const leveldown = require("leveldown");

const dailyDb = levelup(leveldown('./dailyDb'));
const reportsDB = levelup(leveldown("./reportsDB"));

exports.dailyDb = dailyDb;
exports.reportsDB = reportsDB;